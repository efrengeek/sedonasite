<?php
include("components/header.php");
?>

  <!-- Banner starts -->
  <div class="banner-container massage-bg">



        <!-- <div class="banner-description">
          <div class="retreats-banner-title">INTUITIVE READINGS</div>
          <div class="retreats-banner-phar">
            <span>Clarity for Any Situation</span>
          </div>
          <div class="button"><a href="#" class="learn-sedona">MAKE AN APPOINTMENT</a></div><br/>
        </div> -->

    
    

  </div>

<!-- Banner ends -->


<!-- Below Banner starts -->


  <div class="container">
    <div class="row">
      <div class="col-sm-8 no-margin content-text">
        <div class="phar no-padding">   
          <span class="size20"><h2>RELAXATION MASSAGE</h2></span>  
          <span class="size18">
            The Relaxation Massage, often referred to as a Swedish massage, uses smooth, gentle, gliding strokes to help you relax.  
            This light pressure technique is best for anyone looking to bring their stress levels down by receiving gentle and nourishing human touch.  
            Known for its substantial health benefits, this technique stimulates the both the nervous system and the parasympathetic nervous system, 
            causing the brain to release endorphins - the body’s natural feel-good chemicals.  Additional benefits experienced include a decrease in 
            blood pressure and heart rate, and an increase in blood circulation and digestive activity.         
          </span>
        </div>

        <div class="phar no-padding">   
          <div class="size18"><h2 class="font1 italic">What People Are Saying</h2></div>  
          <div class="size16 border-left italic margin-left margin-top">
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"This was a unique experience!" <br/> <br/>  
            <div class="text-right">- Anonymous</div>        
          </div>

        </div>

      </div>

      <div class="col-sm-4 no-padding">
        <!-- ===== SIDE BAR 1 ===== -->
        <div class="phar center no-padding sidebar1">
          <hr class="styled-hr">
          <h2>RELAXATION MASSAGE</h2>
          <h3 class="grayfont">Price: $100 (60 min)</h3>
          <div class="button">
            <a href="#" class="learn-sedona">BOOK ONLINE NOW</a>
          </div><br/>
          <div class="center">
            <span class="size20">
              - OR -<br/>
            </span>
            <span class="size20 bold">
              Call 928-282-3875
            </span>
            <span class="size20"><br/>
              to make an appointment<br/> <br/>
            </span>
          </div>
          <hr class="styled-hr">
        </div>
        <!-- ===== END SIDE BAR 1 ===== -->

        <!-- ===== SIDE BAR 2 ===== -->
        <?php
        include("components/sidebar-bot.php");
        ?>
        <!-- ===== END SIDE BAR 2 ===== -->

      </div>
    </div>
  </div>




  <div class="container margin-bot100 margin-top80">
    <hr class="styled-hr"> 
    <div class="row padding-topbot20">
      <div class="span12 center">
        <span class="size25 font2">Call 928-282-3875 or <a href="">CLICK HERE</a> to make an appointment.</span>
      </div>       
    </div>    
    <hr class="styled-hr">  
  </div>

<!-- Below Banner ends -->


<?php
include("components/footer.php");
?>