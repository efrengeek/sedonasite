<?php
include("components/header.php");
?>


    <!-- Banner starts -->
    <div class="banner-container workshops-bg">

    </div>

    <!-- Banner ends -->


    <div class="container">
        <div class="row">

            <div class="span12 center retreats-section">

                <div class="span3 marg-bot center landing-page-cols-first hbs">
                    <div class="title2 center landing-page-cols-thumb" style="background-image:  url('/img/sedona_images/retreats/heal_your_body_sould.jpg'); ">
                        <div class="title-thumb">
                            <strong>HEAL YOUR BODY & SOUL</strong>
                            <br/>
                            2 days <br/>
                            Customize Healing
                        </div>
                    </div>
                    <div class="text-phar text-left"><span>The Heal Your Body &amp; Soul Weekend Retreat is a customized healing experience that will help you restore balance in your life. <a href="/heal-your-body--soul---2-days.html">Learn more.</a></span></div>
                    <div class="button2 center"><a href="#" class="learn-sedona">Schedule Your Retreat</a></div>
                    <br/>
                </div>

                <div class="span3 marg-bot center landing-page-cols fyp">
                    <div class="title2 center landing-page-cols-thumb" style="background-image:  url('/img/sedona_images/retreats/find_your_purpose.jpg'); ">
                        <div class="title-thumb">
                        <strong>FIND YOUR PURPOSE</strong>
                            <br/>
                            3 or 4 DAYS
                            <br/>
                            Discover Life Passion
                        </div>
                    </div>
                    <div class="text-phar text-left"><span>The Find Your Purpose Retreat will help you figure out what you really want from life, and give you the courage, confidence, and tools to boldly bring that dream to fruition. <a href="/find-your-purpose-3-or-4-days.html">Learn more.</a></span></div>
                    <div class="button2 center"><a href="#" class="learn-sedona">Schedule Your Retreat</a></div>
                    <br/>
                </div>

                <div class="span3 marg-bot center landing-page-cols myd">
                    <div class="title2 center landing-page-cols-thumb" style="background-image:  url('/img/sedona_images/retreats/manifest_your_dream.jpg'); ">
                        <div class="title-thumb">
                            <strong>MANIFEST YOUR DREAM</strong> <br/>
                            5 or 6 DAYS <br/>
                            Dreams Become Reality
                        </div>
                    </div>
                    <div class="text-phar text-left"><span>The Manifest Your Dream Retreat will help you get clear on what you want to create for your life and strengthen your will to actualize those goals and dreams into reality.</span></div>
                    <div class="button2 center"><a href="#" class="learn-sedona">Schedule Your Retreat</a></div>
                    <br/>
                </div>

                <!-- ============================ -->

                <div class="span3 marg-bot center landing-page-cols bmsiw">
                    <div class="title2 center landing-page-cols-thumb" style="background-image:  url('/img/sedona_images/retreats/manifest_your_dream.jpg'); ">
                        <div class="title-thumb">
                            <strong>BODY, MIND, & SPIRIT INTEGRATION WORKSHOP</strong> <br/>
                            Universal Consciousness
                        </div>
                    </div>
                    <div class="text-phar text-left">
                        <span>The Body, Mind, &amp; Spirit Integration Workshop will heighten your senses and raise your consciousness vibration to actualize your highest potential. <a href="/body-mind--spirit-integration.html">Learn more.</a></span>
                    </div>
                    <div class="button2 center"><a href="#" class="learn-sedona">Schedule Your Workshop</a></div>
                    <br/>
                </div>

                <div class="span3 marg-bot center landing-page-cols-first cp">
                    <div class="title2 center landing-page-cols-thumb" style="background-image:  url('/img/sedona_images/retreats/crystal_palace.jpg'); ">
                        <div class="title-thumb">
                        <strong>CRYSTAL PALACE WORKSHOP</strong>
                        <br/>The True Sedona Spirit
                        </div>
                    </div>
                    <div class="text-phar text-left">
                        <span>The Crystal Palace Workshop will allow you to experience the most mystical, awe-inspiring, and sacred energy in all of Sedona. <a href="/crystal-palace---visit--guided-meditation.html">Learn more.</a></span>
                    </div><br/>
                    <div class="button2 center"><a href="#" class="learn-sedona">Schedule Your Workshop</a></div>
                    <br/>
                </div>
                <div class="clearfix"></div>
            </div>

        </div>
    </div>


    <!-- Below Banner ends -->




<?php
include("components/footer.php");
?>