<?php
include("components/header.php");
?>


    <!-- Banner starts -->
    <div class="banner-container index-bg">

        <div class="banner-description">
            <div class="banner-title">Experience the Healing Energy...</div>
            <div class="banner-phar">
            <span> That makes Sedona one of the most famous spiritual<br/> destinations on the planet.
            <br/><br/>
            Sedona Healing Arts offers healing services, retreats,<br/> personalized guidance and products that will help you<br/> recover the health of your body, peace of your heart, and<br/> inspiration of your spirit.</span>
            </div>
            <div class="button"><a href="#" class="learn-sedona">BOOK ONLINE NOW</a></div><br/>
        </div>

    </div>

    <!-- Banner ends -->

    <div class="container">
        <div class="row">
            <div class="col-sm-8">
                <div class="phar no-padding">
                    <span class="size20"><h2>CHAKRA BALANCING AND CRYSTAL HEALING</h2></span>
                    <span class="size20"><br/>
                        A Couple’s Relationship Reading will take your relationship to a new level of connection and understanding.  Whether you are in a budding relationship or one that has withstood the test of time, a couple’s reading will help you clearly understand what each person needs in order to feel valued and understood within the parameters of the relationship, offering insight into the ways in which the foundational dynamics and energy of the relationship can be utilized to bring lasting and unconditional love.  By opening the channels of communication through intuitive guidance you will be able to see your partner in a new light… allowing your relationship to rise to heightened levels of spiritual intimacy. <br/> <br/>          
                    </span>
                </div>

                <div class="phar no-padding">
                    <div class="size20"><h2 class="font1 italic">What People Are Saying</h2></div>
                    <div class="size18 border-left italic margin-left margin-top">
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"This was a unique experience!" <br/> <br/>
                        <div class="text-right">- Anonymous</div>
                    </div>

                </div>
            </div>
            <div class="col-sm-4">
                <br/><br/>
                <!-- ===== SIDE BAR 1 ===== -->
                <div class="phar center no-padding">       
                    <hr class="styled-hr"> 
                    <span class="size18"><h3>CHAKRA BALANCING AND CRYSTAL HEALING</h3></span>      
                    <span class="size18 bold"><br/> 
                    Price: $150 (60 min)<br/> <br/>          
                    </span>    
                    <div class="button"><a href="#" class="learn-sedona">BOOK ONLINE NOW</a></div><br/>    
                    <div class="center">
                    <span class="size20"><br/> 
                      - OR -<br/> <br/>          
                    </span>  
                    <span class="size20 bold">
                      Call 928-282-3875         
                    </span>  
                    <span class="size20"><br/> 
                      to make an appointment<br/> <br/>          
                    </span>    
                    </div>                
                    <hr class="styled-hr">
                </div>
                <!-- ===== END SIDE BAR 1 ===== -->

                <!-- ===== SIDE BAR 2 ===== -->
                <?php
                include("components/sidebar-bot.php");
                ?>
                <!-- ===== END SIDE BAR 2 ===== -->
            </div>
        </div>
    </div>



    <div class="container margin-bot100 margin-top80">
        <hr class="styled-hr">
        <div class="row padding-topbot20">
            <div class="col-sm-12 center">
                <span class="size25 font2">Call 928-282-3875 or <a href="">CLICK HERE</a> to make an appointment.</span>
            </div>
        </div>
        <hr class="styled-hr">
    </div>

    <!-- Below Banner ends -->




<?php
include("components/footer.php");
?>