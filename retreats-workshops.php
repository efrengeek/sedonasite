<?php
  include("components/header.php");
?>
  <!-- Banner starts -->
  <div class="banner-container retreats-bg">



    <!-- <div class="banner-description">
      <div class="retreats-banner-title">INTUITIVE READINGS</div>
      <div class="retreats-banner-phar">
        <span>Clarity for Any Situation</span>
      </div>
      <div class="button"><a href="#" class="learn-sedona">MAKE AN APPOINTMENT</a></div><br/>
    </div> -->




  </div>

  <!-- Banner ends -->

  <div class="container">
    <div class="row">    

      <div class="span9 no-margin center"> 

        <div class="span3 no-margin marg-bot center">   
          <div class="title2 center no-padding margin-top40">
            <hr class="styled-hr"> <br/>
            <p class="size16">HEAL YOUR BODY & SOUL</p>
            <p class="size30 bold-font">2 DAYS</p>
            <p class="size20 thin-font1">Customized Healing</p>
          </div>
          <img src="/img/sedona_images/8050595_orig.jpg">
          <div class="text-phar text-left"><span>The Heal Your Body & Soul Weekend Retreat is a customized healing experience that will help you restore balance in your life. </span></div><br/><br/>
          <div class="button2 center"><a href="#" class="learn-sedona">Learn More</a></div>
          <br/>
          <hr class="styled-hr"> 
        </div>

        <div class="span3 marg-bot center"> 
          <div class="title2 center no-padding margin-top40">
            <hr class="styled-hr"> <br/>
            <p class="size16">FIND YOUR PURPOSE</p>
            <p class="size30 bold-font">3 or 4 DAYS</p>
            <p class="size20 thin-font1">Discover Life Passion</p>
          </div>
          <img src="/img/sedona_images/2293160.jpg">
          <div class="text-phar text-left"><span>The Find Your Purpose Retreat will help you figure out what you really want from life, and give you the courage, confidence, and tools to boldly bring that dream to fruition.</span></div><br/>
          <div class="button2 center"><a href="#" class="learn-sedona">Learn More</a></div>
          <br/>
          <hr class="styled-hr"> 
        </div>

        <div class="span3 marg-bot"> 
          <div class="title2 center no-padding margin-top40">
            <hr class="styled-hr"> <br/>
            <p class="size16">MANIFEST YOUR DREAM</p>
            <p class="size30 bold-font">5 or 6 DAYS</p>
            <p class="size20 thin-font1">Dreams Become Reality</p>
          </div>
          <img src="/img/sedona_images/1553803_orig.jpg">
          <div class="text-phar text-left"><span>The Manifest Your Dream Retreat will help you get clear on what you want to create for your life and strengthen your will to actualize those goals and dreams into reality.</span></div><br/>
          <div class="button2 center"><a href="#" class="learn-sedona">Learn More</a></div>
          <br/>
          <hr class="styled-hr">
        </div>

        <!-- ============================ -->

        <div class="span3 no-margin marg-bot center"> 
          <div class="title2 center no-padding">
            <br/>
            <p class="size16">BODY, MIND, & SPIRIT INTEGRATION WORKSHOP</p>
            <p class="size18 thin-font1">Universal Consciousness</p>
          </div>
          <img src="/img/sedona_images/1553803_orig.jpg">
          <div class="text-phar text-left"><span>The Body, Mind, & Spirit Integration Workshop will heighten your senses and raise your consciousness vibration to actualize your highest potential.</span></div><br/>
          <div class="button2 center"><a href="#" class="learn-sedona">Learn More</a></div>
          <br/>
          <hr class="styled-hr">
        </div>

        <div class="span3 marg-bot center"> 
          <div class="title2 center no-padding">
            <br/>
            <p class="size16">CRYSTAL PALACE WORKSHOP</p>
            <p class="size18 thin-font1">The True Sedona Spirit</p>
          </div>
          <img src="/img/sedona_images/2872673_orig.jpg">
          <div class="text-phar text-left"><span>The Crystal Palace Workshop will allow you to experience the most mystical, awe-inspiring, and sacred energy in all of Sedona.</span></div><br/><br/>
          <div class="button2 center"><a href="#" class="learn-sedona">Learn More</a></div>
          <br/>
          <hr class="styled-hr">
        </div>

      </div>

      <div class="span3"> 
        <div class="phar no-padding">       
          <hr class="styled-hr"> <br/>
           <span class="size25">ALL SERVICES</span>      
          <span class="size16 font2 side-bar-color"><br/><br/> 
            <a href="">HEALING SERVICES</a><br/>          
          </span>   
          <ul>
            <li>Intuitive Reading</li>
            <li>Past Life Reading</li>
            <li>Couple's Relationship Reading</li>
            <li>Chakra Balancing and Crystal Healing</li>
            <li>Spiritual Acupuncture</li>
          </ul>    
          <span class="size16 font2 side-bar-color">
            <a href="">RETREATS & WORKSHOPS</a><br/>          
          </span>   
          <ul>
            <li>Heal Your Body & Soul</li>
            <li>Find Your Purpose</li>
            <li>Manifest Your Dream</li>
            <li>Body, Mind & Spirit Integration</li>
            <li>Crystal Palace Workshop</li>
          </ul>    
          <span class="size16 font2 side-bar-color">
            <a href="">MASSAGE</a><br/>          
          </span>   
          <ul>
            <li>Relaxation Massage</li>
            <li>Meridian Massage</li>
            <li>Deep Tissue Massage</li>
            <li>Hot Stone Massage</li>
            <li>Reflexology</li>
          </ul>                
          <hr class="styled-hr">
        </div>

      </div>
    </div>
  </div>



  <div class="container margin-bot100 margin-top80">
    <hr class="styled-hr"> 
    <div class="row padding-topbot20">
      <div class="span12 center">
        <span class="size25 font2">Call 928-282-3875 or <a href="">CLICK HERE</a> to make an appointment.</span>
      </div>       
    </div>    
    <hr class="styled-hr">  
  </div>

<!-- Below Banner ends -->



<?php
include("components/footer.php");
?>