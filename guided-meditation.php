<?php
include("components/header.php");
?>


    <!-- Banner starts -->
    <div class="banner-container retreats-bg workshops-bg">
        <div class="black-box">
            <span class="banner-title">Guided Meditation</span>
            <br/>
            <span class="banner-sub-title1">The True Spirit of Sedona</span>
            <br/>
            <br/>
            <div class="button"><a href="#" class="learn-sedona">SCHEDULE YOUR WORKSHOP</a></div>
        </div>
    </div>

    <!-- Banner ends -->


    <div class="container">
        <div class="row">

            <div class="col-sm-8 no-margin content-text">

                <div class="phar no-padding">
                    <span class="size20"><h2>GUIDED MEDITATION</h2></span>
                      <p class="size18">
                          This workshop is designed to help you feel energy and go into deeper consciousness. Whether you are a novice or an expert, it can take you deeper into meditation than you've ever gone before.
                      </p>
                </div>

                <div class="phar no-padding">
                    <div class="size18"><h2 class="font1 italic">What People Are Saying</h2></div>
                    <div class="size16 border-left italic margin-left margin-top">
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"This was a unique experience!" <br/> <br/>
                        <div class="text-right">- Anonymous</div>
                    </div>

                </div>

            </div>

            <div class="col-sm-4 no-padding">
                <!-- ===== SIDE BAR 1 ===== -->
                <div class="phar center no-padding sidebar1">
                    <hr class="styled-hr">
                    <h2>GUIDED MEDITATION</h2>
                    <h3 class="grayfont">Cost: $150 (60 min)</h3>
                                <div class="button"><a href="#" class="learn-sedona">BOOK ONLINE NOW</a></div><br/>
                                <div class="center">
                        <span class="size20">
                          - OR -<br/>
                        </span>
                        <span class="size20 bold">
                          Call 928-282-3875
                        </span>
                        <span class="size20"><br/>
                          to make an appointment<br/> <br/>
                        </span>
                    </div>
                    <hr class="styled-hr">
                </div>
                <!-- ===== END SIDE BAR 1 ===== -->

                <!-- ===== SIDE BAR 2 ===== -->
                <?php
                include("components/sidebar-bot.php");
                ?>
                <!-- ===== END SIDE BAR 2 ===== -->

            </div>
        </div>
    </div>



    <!-- Below Banner ends -->




<?php
include("components/footer.php");
?>