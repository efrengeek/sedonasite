<?php
  include("components/header.php");
?>



  <!-- Banner starts -->
  <div class="banner-container index-bg">

    <div class="banner-description">
      <div class="row">
        <div class="col-sm-7">
          <div class="banner-msg">
            <p class="banner-title1">Welcome!</p>
            <div class="banner-phar1">
              <span> Whatever path brought you here, there is a
                reason why you came. Open your ears and
                listen. Listen to the message that Sedona
                has for you.
              </span>
            </div>
            <br/>
            <div class="button"><a href="#" class="learn-sedona">LEARN ABOUT SEDONA HEALING ARTS</a></div><br/>
          </div>
        </div>
        <!-- <div class="col-sm-1"></div> -->
        <div class="col-sm-5">
          <div class="banner-photo">
            <img class="photo" src="/img/sedona_images/photo.jpg">
          </div>
        </div>
      </div>
    </div>

  </div>

<!-- Banner ends -->

<!-- Quete starts -->

  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <div class="phar italic border-left">        
          <span>
            "Banya, on this day, my birthday, I received your beautiful gifts. With gratitude I salute the goodness in you. Namaste."<br/> <br/>          
          </span>        
          <div class="text-right">
            - Nuala Mullan Oct 8, 2013         
          </div>
        </div>
      </div>
    </div>
  </div>

<!-- Quete ends -->

<!-- Pictures Starts -->


  <div class="container">
    <div class="row center">
      <div class="col-sm-4 marg-bot padlr">
        <div class="title2"><span>INTUITIVE READING</span><br/><span class="thin-font">Clarity for Any Situation</span></div><br/>
        <img class="col" src="/img/sedona_images/5745332_orig.jpg">
        <div class="text-phar2 text-left"><span>Intuitive Readings are a powerful tool to bring clarity to any situation; past, current and future relationships, work and career, life purpose, spiritual growth, health, as well as finances. </span></div><br/>
        <div class="button2"><a href="#" class="learn-sedona">MAKE AN APPOINTMENT</a></div>
      </div>
      <div class="col-sm-4 marg-bot padlr">
        <div class="title2"><span>SPIRITUAL ACUPUNCTURE</span><br/><span class="thin-font">Heal Your Root Issues</span></div><br/>
        <img class="col" src="/img/sedona_images/8050595_orig.jpg">
        <div class="text-phar2 text-left"><span>Release the foundational emotional and spiritual trauma that holds physical tension and illness in the cellular memory of the body.</span></div><br/><br/>
        <div class="button2"><a href="#" class="learn-sedona">MAKE AN APPOINTMENT</a></div>
      </div>
      <div class="col-sm-4 marg-bot padlr">
        <div class="title2"><span>CHAKRA HEALING</span><br/><span class="thin-font">Release Energic Tension</span></div><br/>
        <img class="col" src="/img/sedona_images/2872673_orig.jpg">
        <div class="text-phar2 text-left"><span>The Chakra Balancing & Crystal Healing will leave you feeling deeply relaxed, refreshed, hopeful, and renewed.</span></div><br/><br/><br/>
        <div class="button2"><a href="#" class="learn-sedona">MAKE AN APPOINTMENT</a></div>
      </div>
    </div>
  
  </div>

  <div class="container">  
    <hr class="styled-hr">
    <div class="row center">
      <div class="col-sm-6 marg-bot">
        <div class="title4"><span>HEAL YOUR BODY & SOUL</span></div>
        <div class="title5"><span>2 Day Retreat</span></div>
        <div class="title6"><span>Customized Healing Experience</span></div><br/>
        <img class="col" src="/img/sedona_images/img_101.png">
        <div class="text-phar2 text-left"><span>The Heal Your Body & Soul Weekend Retreat is a customized healing experience that will help you restore balance in your life. </span></div><br/><br/>
        <div class="button2"><a href="#" class="learn-sedona">MAKE AN APPOINTMENT</a></div>
      </div>
      <div class="col-sm-6 marg-bot">
        <div class="title4"><span>FIND YOUR PURPOSE</span></div>
        <div class="title5"><span>2 Day Retreat</span></div>
        <div class="title6"><span>Discover Your Life's Passion</span></div><br/>
        <img class="col" src="/img/sedona_images/img_100.png">
        <div class="text-phar2 text-left"><span>The Find Your Purpose Retreat will help you figure out what you really want from life, and give you the courage, confidence, and tools to boldly bring that dream to fruition.</span></div><br/>
        <div class="button2"><a href="#" class="learn-sedona">MAKE AN APPOINTMENT</a></div>
      </div>
    </div>    
    <hr class="styled-hr">
  </div>

  <div class="container">
    <div class="row center">
      <div class="col-sm-5 marg-bot">
        <div class="title7 text-left"><span>Latest Blog Post</span></div><br/>
        <iframe class="index-blog-video" src="https://www.youtube.com/embed/4XIjgXDx7UE" frameborder="0" allowfullscreen></iframe>
      </div>
      <div class="col-sm-7 marg-bot">
        <div class="title8 text-left"><span><br/><br/>Come Celebrate With Us!</span></div>        
        <div class="date text-left"><span>Posted on May 16, 2014</span></div>        
        <div class="text-phar2 text-left">
          <span>Sedona Healing Arts will celebrate its Grand Opening in May 2015! We will keep you posted with more details soon! Stay Tuned!<br/><br/>
            Sedona Healing Arts is a spiritual healing center that offers a wide array of healing services for the body, mind and spirit. We also provide other services such as spiritual readings, acupuncture, retreats, and massage. Sedona Healing Arts also functions as a metaphysical products and souvenirs store. The shop is operated by the most wonderful spiritual guide, healer, and licensed acupuncturist, Banya Lim. ...<a href="">(more)</a></span>
          </div>
        </div>       
        <div class="col-sm-12 title8 text-right">
          <span><a class="anchor" href="">See more posts</a></span>
        </div>
      </div>
      <hr class="styled-hr">
    </div>    
   


<!-- Pictures Ends -->


<!-- Store Info starts -->

  <div class="container">
    <div class="row marg-bot">
        

        <div class="col-sm-2"><br/><br/><br/><br/>
          <img src="/img/sedona_images/sedona_owner.png">
        </div>
        <div class="col-sm-6 title2 center">
          <span class="titlel">From the Owner</span>
        
        <div class="text-left">
          <div class="title2 left">
            <span class="title7">Welcome to Sedona Healing Arts!</span><br/>
            <div class="text-phar2 text-left">
            It's my absolute honor and joy to help you figure out what isn't working in your life, and offer intuitive insight and practical tools to get your 
            life on a track that makes you deliriously happy and completely satisfied.  I look forward to spending time with you and sharing the very special 
            energy of Sedona.  See you soon!
            </div>
          </div><br/>
        </div>
        </div>
        
      <div class="col-sm-4 center">
        <div class="title2">
          <span class="titlel">Special Offer</span>
        </div><br/>
        <img src="/img/sedona_images/3314296_orig.jpg">
      </div>
    </div>    
    <hr class="styled-hr">  
  </div>

  <div class="container">
    <div class="row">
      <div class="col-sm-12 title2">
        <span class="titlel">Store Information</span>
      </div>
    </div>
    <div class="row">      
      <div class="col-sm-8 center">
        <div class="wsite-map"><iframe allowtransparency="true" frameborder="0" scrolling="no" style="width: 100%; height: 250px; margin-top: 10px; margin-bottom: 10px;" src="http://www.weebly.com/ajax/apps/generateMap.php?map=google&elementid=460520539553410645&ineditor=0&control=3&width=auto&height=250px&overviewmap=0&scalecontrol=0&typecontrol=0&zoom=15&long=-111.761684&lat=34.869153&domain=www&point=1&align=1&reseller=false"></iframe></div>
      </div>
      <div class="col-sm-4 text-left">
        <div class="title3">
          <span class="text-top"><span class="titleb">Phone:</span> (928) 282-3875</span> <br/>  
          <span class="text-top"><span class="titleb">Email:</span> contact@sedonastory.com</span> <br/>
          <span class="text-top"><span class="titleb">Hours:</span> Mon - Sun: 10 am - 7 pm</span>  <br/> <br/>
        </div> 
        <div class="title3">
          <span class="titleb text-top">Address:</span> <br/>
          <span class="text-top">207 N State Route 89A</span> <br/>
          <span class="text-top">Sedona, AZ 86336</span>
        </div>      
      </div>    
    </div>    
    <hr class="styled-hr">  
  </div>  

<!-- Store Info ends -->

    <?php
    include("components/footer.php");
    ?>