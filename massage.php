<?php
  include("components/header.php");
?>

  <!-- Banner starts -->
  <div class="banner-container massage-bg">



        <!-- <div class="banner-description">
          <div class="retreats-banner-title">INTUITIVE READINGS</div>
          <div class="retreats-banner-phar">
            <span>Clarity for Any Situation</span>
          </div>
          <div class="button"><a href="#" class="learn-sedona">MAKE AN APPOINTMENT</a></div><br/>
        </div> -->




  </div>

<!-- Banner ends -->


<!-- Below Banner starts -->


  <div class="container">
    <div class="row">    

      <div class="span9 no-margin center"> 

        <div class="span3 no-margin marg-bot center">   
          <div class="title2 center no-padding margin-top40 margin-bot10">
            <hr class="styled-hr"> <br/>
            <p class="size16">RELAXATION MASSAGE</p>
            <p class="size16 thin-font1">Drift Into a State of Peace</p>
          </div>
          <img src="/img/sedona_images/___666502.jpg">
          <div class="text-phar text-left"><span>The Heal Your Body & Soul Weekend Retreat is a customized healing experience that will help you restore balance in your life. </span></div><br/><br/>
          <div class="button2 center"><a href="#" class="learn-sedona">Learn More</a></div>
          <br/>
          <hr class="styled-hr"> 
        </div>

        <div class="span3 marg-bot center"> 
          <div class="title2 center no-padding margin-top40 margin-bot10">
            <hr class="styled-hr"> <br/>
            <p class="size16">MERIDIAN MASSAGE</p>
            <p class="size16 thin-font1">Energy System Reboot</p>
          </div>
          <img src="/img/sedona_images/___5563360.jpg">
          <div class="text-phar text-left"><span>The Find Your Purpose Retreat will help you figure out what you really want from life, and give you the courage, confidence, and tools to boldly bring that dream to fruition.</span></div><br/>
          <div class="button2 center"><a href="#" class="learn-sedona">Learn More</a></div>
          <br/>
          <hr class="styled-hr"> 
        </div>

        <div class="span3 marg-bot"> 
          <div class="title2 center no-padding margin-top40 margin-bot10">
            <hr class="styled-hr"> <br/>
            <p class="size16">DEEP TISSUE MASSAGE</p>
            <p class="size16 thin-font1">The Stress Buster</p>
          </div>
          <img src="/img/sedona_images/_____7617189_orig.jpg">
          <div class="text-phar text-left"><span>The Manifest Your Dream Retreat will help you get clear on what you want to create for your life and strengthen your will to actualize those goals and dreams into reality.</span></div><br/>
          <div class="button2 center"><a href="#" class="learn-sedona">Learn More</a></div>
          <br/>
          <hr class="styled-hr">
        </div>

        <!-- ============================ -->

        <div class="span3 no-margin marg-bot center"> 
          <div class="title2 center no-padding margin-bot10">
            <br/>
            <p class="size16">HOT STONE MASSAGE</p>
            <p class="size16 thin-font1">Warm Earth, Human Touch</p>
          </div>
          <img src="/img/sedona_images/___3976835.jpg">
          <div class="text-phar text-left">
          <span>The Hot Stone Massage is a relaxation massage that incorporates heated, smooth, flat stones to massage the body..</span>
          </div><br/>
          <div class="button2 center"><a href="#" class="learn-sedona">Learn More</a></div>
          <br/>
          <hr class="styled-hr">
        </div>

        <div class="span3 marg-bot center"> 
          <div class="title2 center no-padding margin-bot10">
            <br/>
            <p class="size16">REFLEXOLOGY</p>
            <p class="size16 thin-font1">Trigger Point Stimulation</p>
          </div>
          <img src="/img/sedona_images/_____7617189_orig.jpg">
          <div class="text-phar text-left">
          <span>The Reflexology Massage releases the tension from key areas of the body by compressing trigger points in the feet, hands, or ears.</span>
          </div><br/>
          <div class="button2 center"><a href="#" class="learn-sedona">Learn More</a></div>
          <br/>
          <hr class="styled-hr">
        </div>

      </div>

      <div class="span3"> 
        <div class="phar no-padding">       
          <hr class="styled-hr"> <br/>
           <span class="size25">ALL SERVICES</span>      
          <span class="size16 font2 side-bar-color"><br/><br/> 
            <a href="">HEALING SERVICES</a><br/>          
          </span>     
          <ul>
            <li><a href="">Intuitive Reading</a></li>
            <li><a href="">Past Life Reading</a></li>
            <li><a href="">Couple's Relationship Reading</a></li>
            <li><a href="">Chakra Balancing and Crystal Healing</a></li>
            <li><a href="">Spiritual Acupuncture</a></li>
          </ul>    
          <span class="size16 font2 side-bar-color">
            <a href="">RETREATS & WORKSHOPS</a><br/>          
          </span>   
          <ul>
            <li><a href="">Heal Your Body & Soul</a></li>
            <li><a href="">Find Your Purpose</a></li>
            <li><a href="">Manifest Your Dream</a></li>
            <li><a href="">Body, Mind & Spirit Integration</a></li>
            <li><a href="">Crystal Palace Workshop</a></li>
          </ul>    
          <span class="size16 font2 side-bar-color">
            <a href="">MASSAGE</a><br/>          
          </span>   
          <ul>
            <li><a href="">Relaxation Massage</a></li>
            <li><a href="">Meridian Massage</a></li>
            <li><a href="">Deep Tissue Massage</a></li>
            <li><a href="">Hot Stone Massage</a></li>
            <li><a href="">Reflexology</a></li>
          </ul> 
                         
          <hr class="styled-hr">
        </div>

      </div>
    </div>
  </div>



  <div class="container margin-bot100 margin-top80">
    <hr class="styled-hr"> 
    <div class="row padding-topbot20">
      <div class="span12 center">
        <span class="size25 font2">Call 928-282-3875 or <a href="">CLICK HERE</a> to make an appointment.</span>
      </div>       
    </div>    
    <hr class="styled-hr">  
  </div>

<!-- Below Banner ends -->



<?php
include("components/footer.php");
?>