
<!-- Footer starts -->
<div class="container">
    <div class="row">
        <footer>
            <div class="paragraph" style="text-align:center;">
                <p>
                    <a href="/our-store.html" title="">About Sedona Healing Arts</a>
                    &nbsp; | &nbsp;
                    <a href="/healing-services.html" title="">Healing&nbsp;Services </a>
                    &nbsp; | &nbsp;
                    <a href="/retreats--workshops.html" title="">Retreats &amp; Workshops</a>
                    &nbsp; &nbsp;| &nbsp;&nbsp;
                    <a href="/massage.html" title="">Massage</a><a href="/retreats--workshops.html" title=""></a>
                    &nbsp; &nbsp;|&nbsp;&nbsp;
                    <a href="/massage.html" title="">Shop</a>
                    &nbsp; | &nbsp;
                    <a href="/blog.html" title="">Blog</a>
                    &nbsp;&nbsp; | &nbsp;&nbsp;
                    <a href="/online-reservations.html" title="">Book Online</a>
                    &nbsp; &nbsp;| &nbsp; &nbsp;
                    <a href="/contact.html" title="">Contact Us</a>
                </p> <br/>
                <p class="copy">Copyright © 2015 · All Rights Reserved · Sedona Healing Arts</p>
            </div>
        </footer>
    </div>
</div>

<!-- Footer ends -->

<!-- Scroll to top -->
<span class="totop"><a href="#"><i class="icon-chevron-up"></i></a></span>

<!-- JS -->
<script src="js/jquery.js"></script> <!-- jQuery -->
<script src="js/bootstrap.js"></script> <!-- Bootstrap -->
<script src="js/bootstrap-hover-dropdown.min.js"></script> <!-- Bootstrap Menu -->

<script>
    // very simple to use!
    $(document).ready(function() {
        $('.js-activated').dropdownHover().dropdown();
    });
</script>

</body>
</html>