<!-- ===== SIDE BAR 1 ===== -->
        <div class="phar center no-padding">       
          <hr class="styled-hr"> 
          <span class="size18"><h3>RELAXATION MASSAGE</h3></span>      
          <span class="size18 bold"><br/> 
            Price: $100 (30 min) / $200 (1 hr)<br/> <br/>          
          </span>    
          <div class="button"><a href="#" class="learn-sedona">BOOK ONLINE NOW</a></div><br/>    
          <div class="center">
            <span class="size20"><br/> 
              - OR -<br/> <br/>          
            </span>  
            <span class="size20 bold">
              Call 928-282-3875         
            </span>  
            <span class="size20"><br/> 
              to make an appointment<br/> <br/>          
            </span>    
          </div>                
          <hr class="styled-hr">
        </div>
<!-- ===== END SIDE BAR 1 ===== -->